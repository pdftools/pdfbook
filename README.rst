==========================
pdfbook
==========================

----------------------------------------------------------------
Rearrange pages in PDF file into signatures for printing books.
----------------------------------------------------------------

:Author:  Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version: Version 0.5.0
:Copyright: GNU Public Licence v3 (GPLv3)
:Homepage: http://pdfbook.readthedocs.io/


``Pdfbook`` rearranges pages from a PDF document into "signatures"
for printing books or booklets, creating a new PDF file.

This is much like ``psbook`` does for Postscript files, but working
with PDF. Indeed ``pdfbook`` was inspired by ``psbook``.

For more information please refer to the manpage or visit
the `project homepage <http://pdfbook.readthedocs.io/>`_.


Requirements
~~~~~~~~~~~~~~~~~~~~

``Pdfbook`` requires

* `Python`__  (tested 2.7 and 3.4—3.6, but newer versions should work, too),
* `setuptools`__ or `pip`__ for installation, and
* `PyPDF2`__.

__ https://www.python.org/download/
__ https://pypi.org/project/setuptools
__ https://pypi.org/project/pip
__ http://mstamy2.github.io/PyPDF2/


.. Emacs config:
 Local Variables:
 mode: rst
 End:
