Development
===============================


The source of |pdfbook| and its siblings is maintained at
a decentralized `GitLab` instance. Patches and pull-requests
are hearty welcome.

* Please submit bugs and enhancements to the `Issue Tracker
  <https://gitlab.digitalcourage.de/pdftools/pdfbook/issues>`_.

* You may browse the code at the
  `Repository Browser
  <https://gitlab.digitalcourage.de/pdftools/pdfbook>`_.
  Or you may check out the current version by running ::

    git clone https://gitlab.digitalcourage.de/pdftools/pdfbook.git


*Historical Note:*
|pdfbook| was hosted at origo.ethz.ch, which closed in May 2012.
Then |pdfbook| was hosted on gitorious.org,
which was closed in May 2015 and merged into gitlab.
In 2018 the repository moved to a decentralized GitLab instance.

.. include:: _common_definitions.txt
