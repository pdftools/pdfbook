Frequently Asked Questions
===============================

* Are there other Python tools for manipulating PDF?

  Yes, there are:

  * `pdfposter <https://pypi.org/project/pdftools.pdfposter/>`_
  * `pdfnup <https://pypi.org/project/pdfnup/>`_
  * `pdfsplit <https://pypi.org/project/pdfsplit/>`_
  * `pdfgrid <https://pypi.org/project/pdfgrid/>`_


.. include:: _common_definitions.txt
