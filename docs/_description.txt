
``pdfbook`` can be used to create a large book by building it from
multple pages and/or printing it on large media. It expects as input a
PDF file, normally printing on a single page. The output is again a
PDF file, maybe containing multiple pages together building the
book.

``pdfbook`` rearranges pages from a PDF document into "signatures"
for printing books or booklets, creating a new PDF file.
A signature is a group of pages in a document corresponding to sheets
of paper folded and bound; these pages are normally not in sequential
order in a document. For example, in a document with eight-page
signatures, page 8 and page 1 might both be printed on the same sheet
of paper.

* To rearrange the pages of file ``newsletter.pdf`` into a signature
  and write it to the file ``newsletter.bound.pdf``, type::

    pdfbook newsletter.pdf newsletter.bound.pdf

By default, pdfbook uses one signature for the entire file. If the file
doesn't contain a multiple of four pages, it adds blank pages to the
end.

To specify the size of the signature to use - in other words, the
number of pages that will appear on a single piece of paper - give
the number as an argument to the ``-s`` option. Signature size is
always a multiple of four.

* To rearrange the pages of file ``newsletter.pdf`` into an
  eight-sided signature and write it to ``newsletter.bound.pdf``,
  type::

    pdfbook -s8 newsletter.pdf newsletter.bound.pdf


.. Emacs config:
 Local Variables:
 mode: rst
 End:
