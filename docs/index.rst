==============================================
pdftools.pdfbook
==============================================

.. container:: admonition topic

  **Rearrange pages in PDF file into signatures for printing books.**

|pdfbook| rearranges pages from a PDF document into "signatures"
for printing books or booklets, creating a new PDF file.

This is much like the tool `psbook` does for Postscript files, but
working with PDF. Indeed |pdfbook| was inspired by `psbook`.

.. toctree::
   :maxdepth: 1

   Installation
   Usage
   Examples
   Donate <Donate>
   Frequently Asked Questions
   Changes
   Development

.. include:: _common_definitions.txt
