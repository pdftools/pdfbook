;; Manifest for ``guix shell``
;; Keep aligned with requirements.txt
;;
;; Mind adding this directory to
;; ~/.config/guix/shell-authorized-directories

(specifications->manifest
 '("python-setuptools"
   "python-wheel"
   "python-babel"
   "python-docutils"
   "python-sphinx"
   ;; currently missing in guix: zest.releaser
   "scons"
   ))
