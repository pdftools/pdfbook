==========================
pdfbook
==========================
----------------------------------------------------------------
Rearrange pages in PDF file into signatures for printing books
----------------------------------------------------------------

:Author:    Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version:   Version |VERSION|
:Copyright: 2010-2023 by Hartmut Goebel
:Licence:   GNU Affero General Public License v3 or later (AGPLv3+)
:Manual section: 1

.. raw:: manpage

   .\" disable justification (adjust text to left margin only)
   .ad l


SYNOPSIS
==========

``pdfbook`` <options> infile outfile

DESCRIPTION
============

.. include:: docs/_description.txt


OPTIONS
========

.. include:: docs/_options.txt

EXAMPLES
============

.. include:: docs/_examples.txt

More examples including sample pictures can be found at
https://pdfbook.readthedocs.io/en/latest/Examples.html


SEE ALSO
=============

``psbook``\(1),
``pdfnup``\(1) http://pypi.org/project/pdfnup/,
``pdfsplit``\(1) http://pypi.org/project/pdfsplit/,
``pdfgrid``\(1) http://pypi.org/project/pdfgrid/,
``pdfposter``\(1) https://pdfposter.readthedocs.io/,
``pdfjoin``\(1) https://pdfjoin.readthedocs.io/,
``flyer-composer``\(1) http://www.crazy-compilers.com/flyer-composer.html

Project Homepage https://pdfbook.readthedocs.io/

