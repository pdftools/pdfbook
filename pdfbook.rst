==========================
pdfbook
==========================
----------------------------------------------------------------
Rearrange pages in PDF file into signatures for printing books.
----------------------------------------------------------------

:Author:  Hartmut Goebel <h.goebel@crazy-compilers.com>
:Version: Version 0.1.0
:Copyright: GNU Public Licence v3 (GPLv3)
:Manual section: 1

.. raw:: manpage

   .\" disable justification (adjust text to left margin only)
   .ad l


SYNOPSIS
==========

``pdfbook`` <options> infile outfile

DESCRIPTION
============

.. include:: docs/_description.txt


OPTIONS
========

.. include:: docs/_options.txt

EXAMPLES
============

.. include:: docs/_examples.txt

More examples including sample pictures can be found at
https://pdfbook.readthedocs.io/en/latest/Examples.html


SEE ALSO
=============

``psbook``\(1),
``pdfnup``\(1) http://pypi.org/project/pdfnup/,
``pdfsplit``\(1) http://pypi.org/project/pdfsplit/,
``pdfgrid``\(1) http://pypi.org/project/pdfgrid/,
``pdfposter``\(1) http://pypi.org/project/pdftools.pdfposter/

Project Homepage http://pdfbook.readthedocs.io/

