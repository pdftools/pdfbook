#!/usr/bin/env python
"""
pdftools.pdfbook - rearrange pages in PDF file into signatures.
"""
#
# Copyright 2010-2015 by Hartmut Goebel <h.goebel@crazy-compilers.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

__author__ = "Hartmut Goebel <h.goebel@crazy-compilers.com>"
__copyright__ = "Copyright 2010-2015 by Hartmut Goebel <h.goebel@crazy-compilers.com>"
__licence__ = "GNU General Public License version 3 (GPL v3)"
__version__ = "0.1.0"

from pyPdf.pdf import PdfFileWriter, PdfFileReader, PageObject, \
     NameObject, RectangleObject, NullObject, DictionaryObject

import logging
from logging import log

class DecryptionError(ValueError): pass

PAGE_BOXES = ("/MediaBox", "/CropBox")

class _EmptyPage(PageObject):

    def __init__(self, pdf):
        PageObject.__init__(self, pdf)
        self.__setitem__(NameObject('/Type'), NameObject('/Page'))
        self.__setitem__(NameObject('/Parent'), NullObject())
        self.__setitem__(NameObject('/Resources'), DictionaryObject())
        firstpage = pdf.getPage(0)
        for attr in PAGE_BOXES:
            if firstpage.has_key(attr):
                self[NameObject(attr)] = RectangleObject(list(firstpage[attr]))

def bookify(inpdf, outpdf, signature):

    def sort_pages(numPages, signature):
        maxPage = numPages + (signature - numPages % signature) % signature
        for currentpg in xrange(maxPage):
            actualpg = currentpg - (currentpg % signature)
            if currentpg % 4 in (0, 3):
                actualpg += signature-1 - (currentpg % signature)/2
            else:
                actualpg += (currentpg % signature)/2
            if actualpg >= numPages:
                yield '*', emptyPage
            else:
                yield actualpg+1, inpdf.getPage(actualpg)

    emptyPage = _EmptyPage(inpdf)
    pagelist = []
    for num, page in sort_pages(inpdf.numPages, signature):
        pagelist.append(num)
        outpdf.addPage(page)
    log(19, ' '.join('[%s]' % p for p in pagelist))


def password_hook():
    import getpass
    return getpass.getpass()


def main(opts, infilename, outfilename, password_hook=password_hook):
    logging.basicConfig(level=20-opts.verbose, format="%(message)s")
    outpdf = PdfFileWriter()
    inpdf = PdfFileReader(open(infilename, 'rb'))

    if inpdf.isEncrypted:
        log(16, 'File is encrypted')
        # try empty password first
        if not inpdf.decrypt(''):
            if not inpdf.decrypt(password_hook()):
                raise DecryptionError("Can't decrypt PDF. Wrong Password?")

    log(17, 'Signature :', opts.signature)
    log(18, 'Number of input pages :', inpdf.numPages)

    bookify(inpdf, outpdf, opts.signature)
    if not opts.dry_run:
        outpdf.write(open(outfilename, 'wb'))
