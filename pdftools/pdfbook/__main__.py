#!/usr/bin/env python
"""
pdftools.pdfbook.cmd - rearrange pages in PDF file into signatures.
"""
#
# Copyright 2010-2015 by Hartmut Goebel <h.goebel@crazy-compilers.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

__author__ = "Hartmut Goebel <h.goebel@crazy-compilers.com>"
__copyright__ = "Copyright 2010-2015 by Hartmut Goebel <h.goebel@crazy-compilers.com>"
__licence__ = "GNU General Public License version 3 (GPL v3)"

from . import main, __version__, DecryptionError

def run():
    import argparse
    parser = argparse.ArgumentParser('%prog [options] InputFile OutputFile',
                                     #version=__version__
                                     )
    parser.add_argument('-v', '--verbose', action='count', default=0,
                      help='Be verbose. Tell about number of pages rearranged. Can be used more than once to increase the verbosity. ')
    parser.add_argument('-n', '--dry-run', action='store_true',
                      help='Show what would have been done, but do not generate files.')

    group = parser.add_argument_group('Define Target')
    group.add_argument('-s', '--signature', type=int, default=4,
                     help='Specify the size of the signature (number '
                          'of sides which will be folded and bound together). Default: %(default)s')

    parser.add_argument('inputfile')
    parser.add_argument('ouputfile')

    opts, args = parser.parse_args()

    if opts.signature % 4 != 0 or opts.signature < 0:
        parser.error('-s/--signature <signature> must be positive and divisible by 4.')

    try:
        main(opts, *args)
    except DecryptionError, e:
        raise SystemExit(str(e))


if __name__ == '__main__':
    run()
