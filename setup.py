"""
``Pdfbook`` can be used to create a large book by building it from
multple pages and/or printing it on large media. It expects as input a
PDF file, normally printing on a single page. The output is again a
PDF file, maybe containing multiple pages together building the
book.
The input page will be scaled to obtain the desired size.

This is much like ``book`` does for Postscript files, but working
with PDF. Since sometimes book does not like your files converted
from PDF. :-) Indeed ``pdfbook`` was inspired by ``book``.

For more information please refere to the manpage or visit
the `project homepage <http://pdfbook.readthedocs.io/>`_.
"""

import ez_setup
ez_setup.use_setuptools()

from setuptools import setup, find_packages
additional_keywords ={}

try:
    import py2exe
except ImportError:
    py2exe = None

if py2exe:
    resources = {
        #'other_resources': [(u"VERSIONTAG",1,myrevisionstring)],
        'icon_resources' : [(1,'projectlogo.ico')]
        }
    additional_keywords.update({
        'windows': [],
        'console': [dict(script='pdfbook', **resources)],
        'zipfile': None,
        })

setup(
    name = "pdftools.pdfbook",
    version = "0.1.0",
    #scripts = ['pdfbook'],
    install_requires = ['pyPdf>1.10', 'setuptools'],

    packages=find_packages(exclude=['ez_setup']),
    namespace_packages=['pdftools'],

    package_data = {
        # If any package contains *.txt or *.rst files, include them:
        '': ['*.txt', '*.rst'],
        # And include any *.msg files found in the 'hello' package, too:
        'hello': ['*.msg'],
        },

    # metadata for upload to PyPI
    author = "Hartmut Goebel",
    author_email = "h.goebel@crazy-compilers.com",
    description = "Scale and tile PDF images/pages to print on multiple pages.",
    long_description = __doc__,
    license = "GPL 3.0",
    keywords = "pdf book",
    url          = "http://pdfbook.readthedocs.io/",
    download_url = "https://pypi.org/project/pdftools.pdfbook/",
    classifiers = [
    'Development Status :: 2 - Alpha',
    'Environment :: Console',
    'Intended Audience :: Developers',
    'Intended Audience :: End Users/Desktop',
    'Intended Audience :: System Administrators',
    'License :: OSI Approved :: GNU General Public License (GPL)',
    'Natural Language :: English',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Topic :: Printing',
    'Topic :: Utilities',
    ],

    # these are for easy_install (used by bdist_*)
    zip_safe = True,
    entry_points = {
        "console_scripts": [
            "pdfbook = pdftools.pdfbook.cmd:run",
        ],
    },
    # these are for py2exe
    options = {
        # bundle_files 1: bundle everything, including the Python interpreter 
        # bundle_files 2: bundle everything but the Python interpreter
        # bundle_files 3: don't bundle
       "py2exe":{"optimize": 2,
                 "bundle_files": 1,
                 "includes": [],
                }
        },
    **additional_keywords
)

import glob, os
for fn in glob.glob('*.egg-link'): os.remove(fn)
